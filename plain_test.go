package html2

import (
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/flimzy/testy"
	"golang.org/x/net/html"
)

func TestPlain(t *testing.T) {
	t.Parallel()
	type tt struct {
		opts  []Option
		input string
		want  string
		err   string
	}

	tests := testy.NewTable()
	tests.Add("no markup at all", tt{
		input: "plain text",
		want:  "plain text\n",
	})
	tests.Add("h1", tt{
		input: "<h1>foo</h1>",
		want:  "FOO\n",
	})
	tests.Add("bold", tt{
		input: "<b>bold</b>",
		want:  "bold\n",
	})
	tests.Add("strong", tt{
		input: "<strong>strong</strong>",
		want:  "strong\n",
	})
	tests.Add("ul", tt{
		input: "<ul><li>One</li><li>Two</li></ul>",
		want:  " - One\n - Two\n",
	})
	tests.Add("nested ul", tt{
		input: "<ul><li>One</li><li>Two:<ul><li>a</li><li>b</li></ul></li></ul>",
		want:  " - One\n - Two:\n    * a\n    * b\n",
	})
	tests.Add("ol", tt{
		input: "<ol><li>One</li><li>Two</li></ol>",
		want:  " 1. One\n 2. Two\n",
	})
	tests.Add("nested ol", tt{
		input: "<ol><li>One</li><li>Two:<ol><li>a</li><li>b</li></ol></li></ol>",
		want:  " 1. One\n 2. Two:\n     a. a\n     b. b\n",
	})
	tests.Add("ol ol ul ol", tt{
		input: "<ol><li>One</li><li>Two<ol><li>a</li><li>b<ul><li>x<ol><li>X</li></ol></li></ul></li></ol></li></ol>",
		want:  " 1. One\n 2. Two\n     a. a\n     b. b\n         - x\n            1. X\n",
	})
	tests.Add("italic", tt{
		input: "<i>italic</i>",
		want:  "italic\n",
	})
	tests.Add("emphasis", tt{
		input: "<em>italic</em>",
		want:  "italic\n",
	})
	tests.Add("blink", tt{
		input: "<blink>italic</blink>",
		want:  "italic\n",
	})
	tests.Add("blockquote", tt{
		input: "<blockquote>quoting</blockquote>",
		want:  "> quoting\n",
	})
	tests.Add("anchor", tt{
		input: "<a href='example.com'>link</a>",
		want:  "link\n",
	})
	tests.Add("strikethrough", tt{
		input: "<strike>strike</strike>",
		want:  "strike\n",
	})
	tests.Add("pre", tt{
		input: "<pre>pre   formatted</pre>",
		want:  "pre   formatted\n",
	})
	tests.Add("u", tt{
		input: "<u>underline</u>",
		want:  "underline\n",
	})
	tests.Add("inline quote", tt{
		input: "<q>quote</q>",
		want:  "“quote”\n",
	})
	tests.Add("Ruby", tt{
		input: `<ruby>
		明日 <rp>(</rp><rt>Ashita</rt><rp>)</rp>
		</ruby>`,
		want: "明日 (Ashita)\n",
	})
	tests.Add("inline link", tt{
		input: `Click <a href="here.com">here</a>!`,
		want:  "Click here!\n",
	})
	tests.Add("simple table", tt{
		input: `<table>
				<thead>
					<tr>
						<th>Column 1</th>
						<th>Column 2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>The table body</td>
						<td>with two columns</td>
					</tr>
				</tbody>
			</table>`,
		want: `+----------------+------------------+
|    COLUMN 1    |     COLUMN 2     |
+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
`,
	})
	tests.Add("table no header", tt{
		input: `<table>
				<tbody>
					<tr>
						<td>The table body</td>
						<td>with two columns</td>
					</tr>
				</tbody>
			</table>`,
		want: `+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
`,
	})
	tests.Add("no explicit segments", tt{
		input: `<table>
					<tr>
						<td>The table body</td>
						<td>with two columns</td>
					</tr>
			</table>`,
		want: `+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
`,
	})
	tests.Add("table with footer", tt{
		input: `<table>
				<thead>
					<tr>
						<th>Column 1</th>
						<th>Column 2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>The table body</td>
						<td>with two columns</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td>foot 1</td>
						<td>foot 2</td>
					</tr>
				</tfoot>
			</table>`,
		want: `+----------------+------------------+
|    COLUMN 1    |     COLUMN 2     |
+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
|     FOOT 1     |      FOOT 2      |
+----------------+------------------+
`,
	})
	tests.Add("table with caption", tt{
		input: `<table>
				<caption>An exciting caption</caption>
				<thead>
					<tr>
						<th>Column 1</th>
						<th>Column 2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>The table body</td>
						<td>with two columns</td>
					</tr>
				</tbody>
			</table>`,
		want: `+----------------+------------------+
|    COLUMN 1    |     COLUMN 2     |
+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
An exciting caption
`,
	})
	tests.Add("table with rows outside of tbody", tt{
		input: `<table>
				<thead>
					<tr>
						<th>Column 1</th>
						<th>Column 2</th>
					</tr>
				</thead>
				<tr>
					<td>The table body</td>
					<td>with two columns</td>
				</tr>
			</table>`,
		want: `+----------------+------------------+
|    COLUMN 1    |     COLUMN 2     |
+----------------+------------------+
| The table body | with two columns |
+----------------+------------------+
`,
	})
	tests.Add("descriptions", tt{
		input: `<p>Please use the following paint colors for the new house:</p>

		<dl>
			<dt>Denim (semigloss finish)</dt>
			<dd>Ceiling</dd>
		 
			<dt>Denim (eggshell finish)</dt>
			<dt>Evening Sky (eggshell finish)</dt>
			<dd>Layered on the walls</dd>
		</dl>`,
		want: `Please use the following paint colors for the new house:

Denim (semigloss finish)
  Ceiling
Denim (eggshell finish)
Evening Sky (eggshell finish)
  Layered on the walls
`,
	})
	tests.Add("two paragraphs", tt{
		input: "<p>one</p><p>two</p>",
		want:  "one\n\ntwo\n",
	})
	tests.Add("textarea", tt{
		input: "<textarea>Foo\nBar</textarea>",
		want:  "Foo\nBar\n",
	})
	tests.Add("code", tt{
		input: "This is some <code>code</code>",
		want:  "This is some code\n",
	})
	tests.Add("br", tt{
		input: "<p>foo<br>bar</p><br><br>",
		want:  "foo\nbar\n\n\n",
	})
	tests.Add("hr", tt{
		input: "<p>foo</p><hr><p>bar</p>",
		want:  "foo\n\n ---\n\nbar\n",
	})
	tests.Add("img with alt", tt{
		input: `<img src="foo.gif" alt="Funny image">`,
		want:  "Funny image\n",
	})
	tests.Add("inline img with alt", tt{
		input: `inline <img alt="image"> example.`,
		want:  "inline image example.\n",
	})
	tests.Add("img without alt", tt{
		input: `no <img src="foo.gif"> alt`,
		want:  "no alt\n",
	})
	tests.Add("padding around spans", tt{
		input: "foo <span> bar </span> baz",
		want:  "foo bar baz\n",
	})
	tests.Add("picture with fallback to img", tt{
		input: `<picture><source /><img alt="foo"></picture>`,
		want:  "foo\n",
	})
	tests.Add("footer", tt{
		input: "<footer><p>This is a footer</p></footer>",
		want:  "This is a footer\n",
	})
	tests.Add("heading and paragraph", tt{
		input: `<h1>Heading</h1>

<p>Paragraph.</p>`,
		want: "HEADING\n\nParagraph.\n",
	})
	tests.Add("block quote heading and paragraph", tt{
		input: `<blockquote><h1>Heading</h1>

<p>Paragraph.</p></blockquote>`,
		want: `> HEADING
>
> Paragraph.
`,
	})
	tests.Add("blog example", tt{
		//nolint:revive // allow long lines
		input: `<h1>What are your least favorite things?</h1>

			<p>How can you tell a novice from an expert?</p>
			<p>Of course there are many ways.</p>
			<p>But one simple test I like to use, for example when interviewing developers or engineers for technical roles, is a simple question:</p>
			<blockquote>
			<p>What is your least favorite thing about your favorite [programming language/cloud provider/some other tool]?</p>
			</blockquote>
			<p>I like this question because it very quickly and easily helps separate the novice from the expert—even if I don&rsquo;t understand their answer.</p>
			<p>It&rsquo;s important to ask about <em>their</em> favorite tool.  This avoids the problem that practically everyone can think of a criticism of some tool they&rsquo;ve barely used, if only &ldquo;it&rsquo;s confusing to me.&rdquo;</p>
			<p>Everyone has a favorite tool.  But a novice&rsquo;s favorite may have a difficult time articulating why. Often this enthusiasm has a lot more to do with familiarity than with actual affection for the thing.</p>
			<p>Someone who&rsquo;s only ever used, say, Java, often won&rsquo;t have enough perspective to formulate a criticism.  They likely don&rsquo;t know the different options offered by Python, Ruby, or Haskell.</p>
			<p>In contrast, an expert is much more likely to have consciously bumped into the boundaries inherent in their favorite tool.</p>

				  <hr><i>If you enjoyed this message, <a href='https://jhall.io/daily'>subscribe</a> to <u>The Daily Commit</u> to get future messages to your inbox.</i>`,
		want: `WHAT ARE YOUR LEAST FAVORITE THINGS?

How can you tell a novice from an expert?

Of course there are many ways.

But one simple test I like to use, for example when interviewing developers or engineers for technical roles, is a simple question:

> What is your least favorite thing about your favorite [programming
> language/cloud provider/some other tool]?

I like this question because it very quickly and easily helps separate the novice from the expert—even if I don’t understand their answer.

It’s important to ask about their favorite tool. This avoids the problem that practically everyone can think of a criticism of some tool they’ve barely used, if only “it’s confusing to me.”

Everyone has a favorite tool. But a novice’s favorite may have a difficult time articulating why. Often this enthusiasm has a lot more to do with familiarity than with actual affection for the thing.

Someone who’s only ever used, say, Java, often won’t have enough perspective to formulate a criticism. They likely don’t know the different options offered by Python, Ruby, or Haskell.

In contrast, an expert is much more likely to have consciously bumped into the boundaries inherent in their favorite tool.

 ---

If you enjoyed this message, subscribe to The Daily Commit to get future messages to your inbox.
`,
	})
	tests.Add("quoted list", tt{
		input: `<blockquote>
<ul>
<li>One</li>
<li>Two</li>
</ul>
</blockquote>`,
		want: `> - One
> - Two
`,
	})
	tests.Add("quoted table", tt{
		input: `<blockquote>
<table>
<tr>
<td>One</td><td>Two</td>
</tr>
</table>
</blockquote>`,
		want: `> +-----+-----+
> | One | Two |
> +-----+-----+
`,
	})
	tests.Add("blockquote inside table", tt{
		input: `<table>
	<tr>
		<td>
			<blockquote>Blockquote</blockquote>
		</td>
	</tr>
	</table>`,
		want: `+--------------+
| > Blockquote |
+--------------+
`,
	})
	tests.Add("list inside table", tt{
		input: `<table>
		<tr>
			<td>
				<ol>
					<li>One</li>
					<li>Two</li>
					<li>
						<ol>
							<li>A</li>
							<li>B</li>
						</ol>
					</li>
				</ol>
			</td>
		</tr>
	</table>`,
		want: `+-----------+
|  1. One   |
|  2. Two   |
| 3.  a. A  |
|     b. B  |
+-----------+
`,
	})
	tests.Add("table with long content", tt{
		//nolint:revive // allow long line to test wrapping
		input: `<table>
		<tr>
			<td>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</td>
		</tr>
		</table>`,
		want: `+------------------------------------------------------------------------------+
| Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod      |
| tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, |
| quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo      |
| consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse    |
| cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat    |
| non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. |
+------------------------------------------------------------------------------+
`,
	})
	tests.Add("broken quoting, https://jhall.io/archive/2022/12/07/good-process-bad-process/", tt{
		// debug: true,
		input: `<blockquote>
		<p>A well-designed production process isn&rsquo;t an obstacle to efficient knowledge work, but is instead often a precondition.<br>
		— Cal Newport in <a href="https://amzn.to/3PfTBPt" target="_blank" rel="noopener"><em>A World Without Email</em></a></p>
		</blockquote>`,
		want: `> A well-designed production process isn’t an obstacle to efficient knowledge
> work, but is instead often a precondition.
> — Cal Newport in A World Without Email
`,
	})
	tests.Add("multi-paragraph blockquote", tt{
		input: `<blockquote><p>One</p><p>two</p></blockquote>`,
		want: `> One
>
> two
`,
	})
	tests.Add("br followed by space", tt{
		input: "<br> foo bar",
		want:  "\nfoo bar\n",
	})
	tests.Add("Wrap text in blockquote", tt{
		//nolint:revive // allow long line to test wrapping
		input: `<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote>`,
		want: `> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
> incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
> nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
> eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
> in culpa qui officia deserunt mollit anim id est laborum.
`,
	})
	tests.Add("Wrap text to 50 chars in blockquote", tt{
		opts: []Option{LineWidth(50)},
		//nolint:revive // allow long line to test wrapping
		input: `<blockquote>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote>`,
		want: `> Lorem ipsum dolor sit amet, consectetur
> adipiscing elit, sed do eiusmod tempor
> incididunt ut labore et dolore magna aliqua. Ut
> enim ad minim veniam, quis nostrud exercitation
> ullamco laboris nisi ut aliquip ex ea commodo
> consequat. Duis aute irure dolor in
> reprehenderit in voluptate velit esse cillum
> dolore eu fugiat nulla pariatur. Excepteur sint
> occaecat cupidatat non proident, sunt in culpa
> qui officia deserunt mollit anim id est laborum.
`,
	})
	tests.Add("table with long content, two columns", tt{
		//nolint:revive // allow long line to test wrapping
		input: `<table>
		<tr>
			<td>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</td>
			<td>
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</td>
		</tr>
		</table>`,
		want: `+--------------------------------------+--------------------------------------+
| Lorem ipsum dolor sit amet,          | Duis aute irure dolor in             |
| consectetur adipiscing elit, sed     | reprehenderit in voluptate velit     |
| do eiusmod tempor incididunt ut      | esse cillum dolore eu fugiat nulla   |
| labore et dolore magna aliqua. Ut    | pariatur. Excepteur sint occaecat    |
| enim ad minim veniam, quis nostrud   | cupidatat non proident, sunt in      |
| exercitation ullamco laboris nisi ut | culpa qui officia deserunt mollit    |
| aliquip ex ea commodo consequat.     | anim id est laborum.                 |
+--------------------------------------+--------------------------------------+
`,
	})
	tests.Add("table with long content, three columns", tt{
		//nolint:revive // allow long line to test wrapping
		input: `<table>
		<tr>
			<td>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</td>
			<td>
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
			</td>
			<td>
			Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</td>
		</tr>
		</table>`,
		want: `+-------------------------+-------------------------+-------------------------+
| Lorem ipsum dolor       | Duis aute irure dolor   | Excepteur sint occaecat |
| sit amet, consectetur   | in reprehenderit in     | cupidatat non proident, |
| adipiscing elit, sed    | voluptate velit esse    | sunt in culpa qui       |
| do eiusmod tempor       | cillum dolore eu fugiat | officia deserunt mollit |
| incididunt ut labore    | nulla pariatur.         | anim id est laborum.    |
| et dolore magna         |                         |                         |
| aliqua. Ut enim ad      |                         |                         |
| minim veniam, quis      |                         |                         |
| nostrud exercitation    |                         |                         |
| ullamco laboris nisi ut |                         |                         |
| aliquip ex ea commodo   |                         |                         |
| consequat.              |                         |                         |
+-------------------------+-------------------------+-------------------------+
`,
	})
	tests.Add("table with long content, uneven", tt{
		//nolint:revive // allow long line to test wrapping
		input: `<table>
		<tr>
			<td>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
			</td>
			<td>
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
			</td>
			<td>
			Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</td>
			<td>small</td>
		</tr>
		</table>`,
		want: `+----------------------+----------------------+----------------------+-------+
| Lorem ipsum          | Duis aute            | Excepteur sint       | small |
| dolor sit amet,      | irure dolor in       | occaecat cupidatat   |       |
| consectetur          | reprehenderit in     | non proident, sunt   |       |
| adipiscing elit, sed | voluptate velit      | in culpa qui officia |       |
| do eiusmod tempor    | esse cillum dolore   | deserunt mollit anim |       |
| incididunt ut labore | eu fugiat nulla      | id est laborum.      |       |
| et dolore magna      | pariatur.            |                      |       |
| aliqua. Ut enim ad   |                      |                      |       |
| minim veniam, quis   |                      |                      |       |
| nostrud exercitation |                      |                      |       |
| ullamco laboris nisi |                      |                      |       |
| ut aliquip ex ea     |                      |                      |       |
| commodo consequat.   |                      |                      |       |
+----------------------+----------------------+----------------------+-------+
`,
	})
	tests.Add("bare <li>", tt{
		input: "<li>foo</li>",
		want:  " - foo\n",
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		t.Parallel()

		doc, err := html.Parse(strings.NewReader(tt.input))
		if err != nil {
			t.Fatal(err)
		}
		r, err := RenderPlainText(tt.opts...)
		if err != nil {
			t.Fatal(err)
		}
		got, err := RenderToString(doc, r)
		if !testy.ErrorMatches(tt.err, err) {
			t.Errorf("Unexpected error: %s", err)
		}
		if d := cmp.Diff(tt.want, got); d != "" {
			t.Error(d)
		}
	})
}

func FuzzRenderPlainText(f *testing.F) {
	f.Add("<p>test</p>")
	f.Add("<h1>heading</h1><i>italic</i>")
	f.Add("<table>")
	f.Add(`<picture><source /><img alt="foo"></picture>`)
	f.Add("<ol><li>One</li><li>Two:<ol><li>a</li><li>b</li></ol></li></ol>")
	f.Add("</h1>")

	r, err := RenderPlainText()
	if err != nil {
		panic(err)
	}

	f.Fuzz(func(t *testing.T, s string) {
		doc, err := html.Parse(strings.NewReader(s))
		if err != nil {
			t.Skip()
		}
		_, err = RenderToString(doc, r)
		if err != nil {
			t.Fatal(err)
		}
	})
}
