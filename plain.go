package html2

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"reflect"
	"regexp"
	"strings"
	"sync"

	"github.com/olekukonko/tablewriter"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

type plain struct {
	lineWidth int
	ulBullets []string
	olBullets []OLIterator
	prefixes  map[uintptr]func() string
	bullets   map[uintptr]func() string
}

var _ Renderer = &plain{}

// RenderPlainText returns a renderer that returns plain text output.
func RenderPlainText(opts ...Option) (Renderer, error) {
	p := &plain{
		lineWidth: DefaultLineWidth,
		ulBullets: []string{"-", "*", "+"},
		olBullets: []OLIterator{&Numbers{}, &LettersLower{}, &RomanLower{}, &LettersUpper{}, &RomanUpper{}},
		prefixes:  make(map[uintptr]func() string),
		bullets:   make(map[uintptr]func() string),
	}

	for _, opt := range opts {
		if err := opt.apply(p); err != nil {
			return nil, err
		}
	}

	return p, nil
}

func (p *plain) clone() *plain {
	return &plain{
		lineWidth: p.lineWidth,
		ulBullets: p.ulBullets,
		olBullets: p.olBullets,
		prefixes:  make(map[uintptr]func() string),
		bullets:   make(map[uintptr]func() string),
	}
}

func (p *plain) setPrefix(n *html.Node, fn func() string) {
	p.prefixes[reflect.ValueOf(n).Pointer()] = fn
}

func (p *plain) prefix(n *html.Node) string {
	parts := []string{}
	for node := n; node != nil; node = node.Parent {
		if fn, ok := p.prefixes[reflect.ValueOf(node).Pointer()]; ok {
			parts = append([]string{fn()}, parts...)
		}
	}
	return strings.Join(parts, "")
}

func (p *plain) setBullet(n *html.Node, fn func() string) {
	p.bullets[reflect.ValueOf(n).Pointer()] = fn
}

func (p *plain) bullet(n *html.Node) string {
	for node := n; node != nil; node = node.Parent {
		if fn, ok := p.bullets[reflect.ValueOf(node).Pointer()]; ok {
			return fn()
		}
	}
	// If a <li> happens at the top level of a doc, it should be
	// treated as a member of an <ul>
	return p.ulBullets[0]
}

// isBlockStart returns true if n should be rendered as the beginning of a
// new block.
func isBlockStart(n *html.Node) (ok, padding bool) {
	return _isBlockStart(n)
}

func _isBlockStart(n *html.Node) (ok, padding bool) {
	if ok, padding := isBlockElement(n.DataAtom); ok {
		// This element itself starts a block
		return ok, padding
	}
	if s := n.PrevSibling; s != nil && len(strings.TrimSpace(s.Data)) > 0 {
		return isBlockElement(s.DataAtom)
	}
	if p := n.Parent; p != nil {
		return _isBlockStart(p)
	}

	// If we get all the way to the top, it means we're the first display
	// element in the document, so we'll treat the document as a de facto
	// block.
	return true, true
}

// isBlockEnd returns true if n should be rendered as the end of a block.
func isBlockEnd(n *html.Node) (ok, padding bool) {
	if n != nil {
		return _isBlockEnd(n)
	}
	return false, false
}

func _isBlockEnd(n *html.Node) (ok, padding bool) {
	if ok, padding := isBlockElement(n.DataAtom); ok {
		// This element itself ends a block
		return ok, padding
	}

	if s := n.NextSibling; s != nil && len(strings.TrimSpace(s.Data)) > 0 {
		return isBlockElement(s.DataAtom)
	}

	if p := n.Parent; p != nil {
		return _isBlockEnd(p)
	}

	// If we get all the way to the top, it means we're the first display
	// element in the document, so we'll treat the document as a defacto
	// block.
	return true, true
}

// isBlockElement returns ok if typ is a block element, and padded if it should
// be padded with a blank line.
func isBlockElement(typ atom.Atom) (ok, padded bool) {
	switch typ {
	// List taken from https://www.w3schools.com/html/html_blocks.asp
	case atom.P, atom.Article, atom.Aside, atom.Blockquote, atom.H1, atom.H2,
		atom.H3, atom.H4, atom.H5, atom.H6, atom.Table, atom.Ol, atom.Ul,
		atom.Pre, atom.Section, atom.Address, atom.Hr:
		return true, true
	case atom.Canvas, atom.Dd, atom.Div, atom.Dl, atom.Dt, atom.Fieldset,
		atom.Figcaption, atom.Figure, atom.Footer, atom.Form, atom.Header,
		atom.Li, atom.Main, atom.Nav, atom.Noscript, atom.Tfoot, atom.Video,
		atom.Body,
		// These aren't really block displays, but we treat them as such
		atom.Td, atom.Th, atom.Br:
		return true, false
	}
	return false, false
}

//nolint:revive // allow flag parameter
func (p *plain) Render(w Writer, n *html.Node, entering bool) error {
	if !entering {
		defer func() {
			ptr := reflect.ValueOf(n).Pointer()
			delete(p.prefixes, ptr)
			delete(p.bullets, ptr)
		}()
	}
	switch n.Type {
	case html.ErrorNode, html.DocumentNode, html.CommentNode, html.DoctypeNode, html.RawNode:
		return nil
	case html.TextNode:
		return p.renderText(w, n, entering)
	case html.ElementNode:
		switch n.DataAtom {
		case atom.Body:
			return p.renderBody(w, n, entering)
		case atom.P:
			return p.renderParagraph(w, n, entering)
		case atom.H1, atom.H2, atom.H3, atom.H4, atom.H5, atom.H6:
			return p.renderHeading(w, n, entering)
		case atom.B, atom.Strong:
			return p.renderBold(w, n, entering)
		case atom.Strike, atom.S, atom.Del:
			return p.renderStrike(w, n, entering)
		case atom.Q:
			return p.renderInlineQuote(w, n, entering)
		case atom.U:
			return p.renderUnderline(w, n, entering)
		case atom.I, atom.Em, atom.Blink:
			return p.renderItalic(w, n, entering)
		case atom.Dl:
			return nil
		case atom.Dt:
			return p.renderDt(w, n, entering)
		case atom.Dd:
			return p.renderDd(w, n, entering)
		case atom.Ul, atom.Ol:
			return p.renderList(w, n, entering)
		case atom.Li:
			return p.renderListItem(w, n, entering)
		case atom.Blockquote:
			return p.renderBlockquote(w, n, entering)
		case atom.A:
			return p.renderAnchor(w, n, entering)
		case atom.Pre, atom.Textarea:
			return p.renderPre(w, n, entering)
		case atom.Code, atom.Samp, atom.Kbd, atom.Var, atom.Tt:
			return p.renderMonospace(w, n, entering)
		case atom.Br:
			return p.renderBreak(w, n, entering)
		case atom.Hr:
			return p.renderHorizRule(w, n, entering)

		// Images
		case atom.Img:
			return p.renderImage(w, n, entering)

		// Table handling
		case atom.Table:
			return p.renderTable(w, n, entering)
		// These are rendered independently, and passed to the table generator
		case atom.Th, atom.Td:
			return nil
		// These should never actually be called directly.
		case atom.Thead, atom.Tfoot, atom.Tbody:
			panic("bug: table element should not be rendered")

		// See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ruby
		case atom.Ruby, atom.Rp, atom.Rt:
			return nil

		// These elements should not be rendered at all.
		case atom.Head, atom.Applet, atom.Map, atom.Meta, atom.Iframe, atom.Audio,
			atom.Video, atom.Basefont, atom.Frame, atom.Frameset, atom.Canvas,
			atom.Style, atom.Svg, atom.Progress, atom.Colgroup, atom.Col,
			atom.Title, atom.Source, atom.Button, atom.Datalist, atom.Select,
			atom.Label, atom.Dialog, atom.Dir, atom.Embed, atom.Fieldset,
			atom.Figure, atom.Figcaption, atom.Input, atom.Legend, atom.Meter,
			atom.Nav, atom.Optgroup, atom.Option, atom.Param, atom.Template,
			atom.Base, atom.Wbr:
			if entering {
				return ErrWalkSkipChildren
			}
			return nil
		// These elements should be rendered as normal text.
		case atom.Html, atom.Area, atom.Link, atom.Script, atom.Data, atom.Abbr,
			atom.Acronym, atom.Address, atom.Article, atom.Aside, atom.Div,
			atom.Span, atom.Big, atom.Small, atom.Sub, atom.Sup, atom.Cite,
			atom.Time, atom.Summary, atom.Bdi, atom.Bdo, atom.Picture,
			atom.Center, atom.Form, atom.Details, atom.Dfn, atom.Font,
			atom.Footer, atom.Header, atom.Ins, atom.Main, atom.Mark,
			atom.Noframes, atom.Noscript, atom.Object, atom.Output,
			atom.Section:
			return nil
			/*
				Form things:
					Button                    Atom = 0x19106
					Datalist                  Atom = 0x4a508
					Form                      Atom = 0x26e04
					Select                    Atom = 0x63c06
					Label                     Atom = 0x5905
					Fieldset                  Atom = 0x22608
					Input                     Atom = 0x44b05
					Legend                    Atom = 0x18106
					Optgroup                  Atom = 0x5f08
					Option                    Atom = 0x6e306

				Might be useful for metadata in the future
					Base                      Atom = 0x3b04
					Wbr                       Atom = 0x57c03
			*/
		}
	}
	fmt.Fprintf(os.Stderr, "[%t] %v: %s %s\n", entering, n.Type, n.DataAtom, n.Data)
	return nil
}

var (
	trimSpacePrefixRE = regexp.MustCompile(`^[\s\p{Zs}]+`)
	trimSpaceSuffixRE = regexp.MustCompile(`[\s\p{Zs}]+$`)
	collapseSpacesRE  = regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	onlySpacesRE      = regexp.MustCompile(`^[\s\p{Zs}]+$`)
)

func isPreformatted(n *html.Node) bool {
	if n == nil {
		return false
	}
	switch n.DataAtom {
	case 0:
		return isPreformatted(n.Parent)
	case atom.Pre:
		return true
	}
	return false
}

//nolint:revive // allow flag parameter
func (p *plain) renderText(w Writer, n *html.Node, entering bool) error {
	if !entering {
		return nil
	}

	data := n.Data
	if onlySpacesRE.MatchString(data) {
		return nil
	}

	start, startPadding := isBlockStart(n)
	lastByte, ok := w.LastByte()
	if startPadding {
		if ok {
			_ = w.WriteByte('\n')
			if lastByte != '\n' {
				_, _ = w.WriteString(p.prefix(n) + "\n")
			}
		}
	}
	if start || lastByte == '\n' {
		_, _ = w.WriteString(p.prefix(n))
	}

	if !isPreformatted(n) {
		lastByte, _ := w.LastByte()
		if start || lastByte == ' ' {
			data = trimSpacePrefixRE.ReplaceAllString(data, "")
		}
		if end, _ := isBlockEnd(n); end {
			data = trimSpaceSuffixRE.ReplaceAllString(data, "")
		}
		data = collapseSpacesRE.ReplaceAllString(data, " ")
	}

	_, _ = w.WriteString(data)
	return nil
}

func (p *plain) renderHeading(w Writer, n *html.Node, entering bool) error { //nolint:revive // allow control flag
	if !entering {
		return nil
	}
	child := new(html.Node)
	if n.FirstChild == nil {
		return nil
	}
	*child = *n.FirstChild
	child.Parent = nil

	heading, _ := RenderToString(child, p.clone())
	_, _ = fmt.Fprintf(w, "%s%s", p.prefix(n), strings.ToUpper(heading))

	return ErrWalkSkipChildren
}

func (*plain) renderBold(Writer, *html.Node, bool) error {
	// TODO: Optional formatting
	return nil
}

func (*plain) renderItalic(Writer, *html.Node, bool) error {
	// TODO: Optional formatting
	return nil
}

func (*plain) renderStrike(Writer, *html.Node, bool) error {
	// TODO: Optional formatting
	return nil
}

func (*plain) renderUnderline(Writer, *html.Node, bool) error {
	// TODO: Optional formatting
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderList(_ Writer, n *html.Node, entering bool) error {
	if entering {
		p.setBullet(n, p.listBullet(n))
	}
	return nil
}

// listDepth returns the depth of the same type of list (<ul> or <ol>), used
// to determine the bulleting style.
func listDepth(n *html.Node) int {
	depth := 1
	for p := n.Parent; p != nil; p = p.Parent {
		switch p.DataAtom {
		case n.DataAtom:
			depth++
		case atom.Ul, atom.Ol:
			return depth
		}
	}
	return depth
}

func (p *plain) listBullet(n *html.Node) func() string {
	depth := listDepth(n)

	switch n.DataAtom {
	case atom.Ul:
		b := p.ulBullets[len(p.ulBullets)%depth]
		return func() string {
			return b
		}
	case atom.Ol:
		iter := p.olBullets[len(p.olBullets)%depth]
		return iter.New().Next
	case atom.Dl:
		return func() string {
			return ""
		}
	}
	panic(fmt.Sprintf("Unexpected list type: %s", n.DataAtom))
}

//nolint:revive // allow control flag
func (p *plain) renderListItem(w Writer, n *html.Node, entering bool) error {
	if entering {
		last, ok := w.LastByte()
		if ok && last != '\n' {
			_ = w.WriteByte('\n')
		}
		if bullet := p.bullet(n); len(bullet) > 0 {
			var i int
			p.setPrefix(n, func() string {
				if i == 0 {
					i++
					return fmt.Sprintf(" %s ", bullet)
				}
				// TODO: Consider padding this to the size of the longest bullet.
				return strings.Repeat(" ", len(bullet)+2)
			})
		}
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderDt(w Writer, n *html.Node, entering bool) error {
	if entering {
		if start, _ := isBlockStart(n); start {
			_ = w.WriteByte('\n')
		}
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderDd(w Writer, n *html.Node, entering bool) error {
	if entering {
		if start, _ := isBlockStart(n); start {
			_ = w.WriteByte('\n')
		}
		p.setPrefix(n, func() string {
			return "  "
		})
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderBlockquote(w Writer, n *html.Node, entering bool) error {
	if !entering {
		return nil
	}

	if _, ok := w.LastByte(); ok {
		// If this isn't the beginning of the output, precede the
		// blockquote with a newline.
		_ = w.WriteByte('\n')
	}

	buf := &bytes.Buffer{}
	err := Render(buf, n.FirstChild, p.clone())
	if err != nil {
		return err
	}
	r := bufio.NewReader(buf)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			return err
		}
		line = strings.TrimSuffix(line, "\n")
		if len(line) > 0 {
			prefix := p.prefix(n)
			preLen := len(prefix)
			b := strings.Builder{}
			for _, word := range strings.Fields(line) {
				if preLen+2+b.Len()+1+len(word) > p.lineWidth {
					_, _ = fmt.Fprintf(w, "%s> %s\n", prefix, b.String())
					b.Reset()
				}
				if b.Len() > 0 {
					b.WriteByte(' ')
				}
				b.WriteString(word)
			}

			if b.Len() > 0 {
				_, _ = fmt.Fprintf(w, "%s> %s\n", prefix, b.String())
			}
		} else if err != io.EOF {
			_, _ = fmt.Fprintf(w, "%s>\n", p.prefix(n))
		}
		if err == io.EOF {
			break
		}
	}

	return ErrWalkSkipChildren
}

//nolint:revive // allow control flag
func (p *plain) renderAnchor(w io.Writer, n *html.Node, entering bool) error {
	// TODO: Optionally include links
	return nil
}

func (*plain) renderPre(Writer, *html.Node, bool) error {
	return nil
}

func (*plain) renderMonospace(Writer, *html.Node, bool) error {
	// TODO: Optional formatting?
	return nil
}

//nolint:revive // allow control flag
func (*plain) renderInlineQuote(w Writer, _ *html.Node, entering bool) error {
	if entering {
		_, _ = w.WriteString("“")
		return nil
	}
	_, _ = w.WriteString("”")
	return nil
}

//nolint:revive // allow control flag
func (*plain) renderParagraph(w Writer, _ *html.Node, entering bool) error {
	if !entering {
		_ = w.WriteByte('\n')
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderBody(w Writer, _ *html.Node, entering bool) error {
	if !entering {
		// Make sure output always ends with a \n
		if last, _ := w.LastByte(); last != '\n' {
			_ = w.WriteByte('\n')
		}
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderTable(w Writer, n *html.Node, entering bool) error {
	if !entering {
		return nil
	}

	targetWidth := p.lineWidth - len(p.prefix(n))
	tryWidth := targetWidth

	var lastRender *bytes.Buffer
	var lastWidth int
	for {
		buf := &bytes.Buffer{}
		colCount := p.renderRawTable(buf, n, tryWidth)

		width := len(bytes.SplitN(buf.Bytes(), []byte{'\n'}, 2)[0])
		if width >= targetWidth && lastRender != nil {
			break
		}
		lastRender = buf
		if width == lastWidth || width == targetWidth {
			break
		}
		lastWidth = width
		tryWidth += colCount
	}

	r := bufio.NewReader(lastRender)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			return err
		}
		_, _ = fmt.Fprintf(w, "%s%s", p.prefix(n), line)
		if err == io.EOF {
			break
		}
	}

	return ErrWalkSkipChildren
}

// renderRawTable renders the table to w. It returns the number of rendered
// columns, to serve as a hint as to whether this should be re-rendered with
// a different width target.
func (p *plain) renderRawTable(w io.Writer, n *html.Node, width int) int {
	table := tablewriter.NewWriter(w)

	var cols int
	var once sync.Once
	setWidth := func(colCount int) {
		once.Do(func() {
			cols = colCount
			padding := 1 + colCount*3
			table.SetColWidth((width - padding) / colCount)
		})
	}

	_ = walk(n.FirstChild, func(n *html.Node, entering bool) error {
		if !entering {
			return nil
		}
		if n.FirstChild == nil {
			return nil
		}
		child := new(html.Node)
		*child = *n.FirstChild
		child.Parent = nil
		switch n.DataAtom {
		case atom.Thead:
			data := p.tableRow(child)
			setWidth(len(data))
			table.SetHeader(data)
			return ErrWalkSkipChildren
		case atom.Tfoot:
			data := p.tableRow(child)
			setWidth(len(data))
			table.SetFooter(data)
			return ErrWalkSkipChildren
		case atom.Tbody:
			data := p.tableRows(child)
			setWidth(len(data[0]))
			table.AppendBulk(data)
			return ErrWalkSkipChildren
		case atom.Caption:
			caption, _ := RenderToString(child, p.clone())
			table.SetCaption(true, caption)
			return ErrWalkSkipChildren
		}
		return nil
	})

	table.Render()
	return cols
}

// contains returns true if n is or contains a node of type typ.
func contains(n *html.Node, typ ...atom.Atom) bool {
	if n == nil {
		return false
	}
	for _, t := range typ {
		if n.DataAtom == t {
			return true
		}
	}
	if contains(n.FirstChild, typ...) {
		return true
	}
	return contains(n.NextSibling, typ...)
}

func (p *plain) tableRows(n *html.Node) [][]string {
	trs := []*html.Node{}
	_ = walk(n, func(n *html.Node, entering bool) error {
		if entering {
			switch n.DataAtom {
			case atom.Thead, atom.Tfoot:
				return ErrWalkSkipChildren
			}
			if n.DataAtom == atom.Tr && contains(n, atom.Td, atom.Th) {
				trs = append(trs, n)
			}
		}
		return nil
	})

	rows := make([][]string, len(trs))
	for i, tr := range trs {
		rows[i] = p.tableRow(tr)
	}
	return rows
}

func (p *plain) tableRow(n *html.Node) []string {
	values := []string{}
	_ = walk(n, func(n *html.Node, entering bool) error {
		if entering {
			switch n.DataAtom {
			case atom.Td, atom.Th:
				value, _ := RenderToString(n.FirstChild, p.clone())
				values = append(values, strings.TrimSuffix(value, "\n"))
			}
		}
		return nil
	})
	return values
}

//nolint:revive // allow control flag
func (p *plain) renderBreak(w Writer, _ *html.Node, entering bool) error {
	if entering {
		_ = w.WriteByte('\n')
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderHorizRule(w Writer, _ *html.Node, entering bool) error {
	if entering {
		if last, ok := w.LastByte(); ok && last != '\n' {
			_ = w.WriteByte('\n')
		}
		_, _ = w.WriteString("\n ---")
	}
	return nil
}

//nolint:revive // allow control flag
func (p *plain) renderImage(w Writer, n *html.Node, entering bool) error {
	if entering {
		for _, attr := range n.Attr {
			if attr.Key == "alt" {
				_, _ = w.WriteString(attr.Val)
				return nil
			}
		}
	}
	return nil
}
