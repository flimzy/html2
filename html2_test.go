package html2

import (
	"io"
	"strings"
	"testing"

	"golang.org/x/net/html"
)

func Test_walk(t *testing.T) {
	input := `<html><body><h1>heading</h1><p>Paragraph</p></body></html>`

	doc, err := html.Parse(strings.NewReader(input))
	if err != nil {
		t.Fatal(err)
	}
	var nodes int
	err = Render(io.Discard, doc, RenderFunc(func(_ Writer, _ *html.Node, entering bool) error {
		if entering {
			nodes++
		}
		return nil
	}))
	if err != nil {
		t.Fatal(err)
	}
	wantNodes := 8
	if wantNodes != nodes {
		t.Errorf("Expected to walk %d nodes, but walked %d", wantNodes, nodes)
	}
}
