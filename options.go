package html2

import "errors"

// Option represents an option to tweak the behavior of a Renderer.
type Option interface {
	apply(Renderer) error
}

// DefaultLineWidth is the maximum line width by default. It affects linebreaks
// in blockquotes, and the maximum table width. Use LineWidth() to override.
const DefaultLineWidth = 80

type lineWidth int

func (w lineWidth) apply(r Renderer) error {
	p, ok := r.(*plain)
	if !ok {
		return errors.New("use LineWidth for plain text renderer")
	}
	p.lineWidth = int(w)
	return nil
}

// LineWidth configures the maximum width for the plain text renderer.
func LineWidth(width int) Option {
	return lineWidth(width)
}
