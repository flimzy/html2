package html2

import (
	"fmt"
	"math"
	"strings"
)

// OLIterator returns numbered list counters.
type OLIterator interface {
	// New returns a new instance of the OLSeries.
	New() OLSeries
}

// OLSeries iterantes over sequential ordered list identifiers.
type OLSeries interface {
	// Next returns the next iterator label, including any trailing punctuation
	// such as a period (e.g. "1.", "2."...)
	Next() string
}

// Numbers is an OLIterator that returns e.g. 1., 2., 3....
type Numbers struct {
	i int
}

var (
	_ OLIterator = &Numbers{}
	_ OLSeries   = &Numbers{}
)

// New returns satisfies the OLIterator interface.
func (*Numbers) New() OLSeries {
	return &Numbers{}
}

// Next returns the next number in the series.
func (n *Numbers) Next() string {
	n.i++
	return fmt.Sprintf("%d.", n.i)
}

// LettersLower is an OLIterator that returns e.g. a., b., c....
type LettersLower struct{}

var _ OLIterator = &LettersLower{}

// New returns satisfies the OLIterator interface.
func (*LettersLower) New() OLSeries {
	return newLetters('a')
}

// LettersUpper is an OLIterator that returns e.g. A., B., C....
type LettersUpper struct{}

var _ OLIterator = &LettersLower{}

// New returns satisfies the OLIterator interface.
func (*LettersUpper) New() OLSeries {
	return newLetters('A')
}

type letters struct {
	i    int
	base byte
}

func newLetters(first byte) OLSeries {
	return &letters{
		base: first - 1,
	}
}

var log26 = math.Log(26)

func (n *letters) digits() int {
	if n.i == 1 {
		return 1
	}
	return int(math.Ceil(math.Log(float64(n.i)) / log26))
}

func (n *letters) Next() string {
	n.i++
	digits := n.digits()
	parts := make([]byte, digits)
	for i := 0; i < digits; i++ {
		c := n.i / int(math.Pow(26, float64(digits-i-1)))
		if c > 26 {
			c %= 26
		}
		parts[i] = n.base + byte(c)
	}
	return string(parts) + "."
}

// RomanLower is an OLIterator that returns e.g. i., ii., iii....
type RomanLower struct{}

var _ OLIterator = &RomanLower{}

// New returns satisfies the OLIterator interface.
func (*RomanLower) New() OLSeries {
	return newRoman(false)
}

// RomanUpper is an OLIterator that returns e.g. I., II., III....
type RomanUpper struct{}

var _ OLIterator = &RomanUpper{}

// New returns satisfies the OLIterator interface.
func (*RomanUpper) New() OLSeries {
	return newRoman(true)
}

var romanConversions = []struct {
	value int
	digit string
}{
	{1000, "M"},
	{900, "CM"},
	{500, "D"},
	{400, "CD"},
	{100, "C"},
	{90, "XC"},
	{50, "L"},
	{40, "XL"},
	{10, "X"},
	{9, "IX"},
	{5, "V"},
	{4, "IV"},
	{1, "I"},
}

type roman struct {
	i     int
	upper bool
}

func newRoman(upper bool) OLSeries {
	return &roman{upper: upper}
}

func (n *roman) Next() string {
	n.i++
	if n.i > 3999 {
		// Roman numerals max out at 3999, so in case someone is crazy enough
		// to have this many list items, fall back to decimal.
		return fmt.Sprintf("%d.", n.i)
	}

	// algorithm borrowed from https://freshman.tech/snippets/go/roman-numerals/
	x := n.i
	var result strings.Builder
	for _, conversion := range romanConversions {
		for x >= conversion.value {
			result.WriteString(conversion.digit)
			x -= conversion.value
		}
	}
	if n.upper {
		return result.String() + "."
	}
	return strings.ToLower(result.String()) + "."
}
