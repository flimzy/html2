module gitlab.com/flimzy/html2

go 1.19

require (
	github.com/google/go-cmp v0.5.9
	github.com/olekukonko/tablewriter v0.0.5
	gitlab.com/flimzy/testy v0.12.2
	golang.org/x/net v0.2.0
)

require (
	github.com/Masterminds/semver/v3 v3.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/otiai10/copy v1.7.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
