package html2

import (
	"strconv"
	"testing"
)

func TestOLNumbering(t *testing.T) {
	tests := []struct {
		i            int
		numbers      string
		lettersLower string
		lettersUpper string
		romanLower   string
		romanUpper   string
	}{
		{
			i:            1,
			numbers:      "1.",
			lettersLower: "a.",
			lettersUpper: "A.",
			romanLower:   "i.",
			romanUpper:   "I.",
		},
		{
			i:            2,
			numbers:      "2.",
			lettersLower: "b.",
			lettersUpper: "B.",
			romanLower:   "ii.",
			romanUpper:   "II.",
		},
		{
			i:            26,
			numbers:      "26.",
			lettersLower: "z.",
			lettersUpper: "Z.",
			romanLower:   "xxvi.",
			romanUpper:   "XXVI.",
		},
		{
			i:            27,
			numbers:      "27.",
			lettersLower: "aa.",
			lettersUpper: "AA.",
			romanLower:   "xxvii.",
			romanUpper:   "XXVII.",
		},
		{
			i:            1000,
			numbers:      "1000.",
			lettersLower: "all.",
			lettersUpper: "ALL.",
			romanLower:   "m.",
			romanUpper:   "M.",
		},
		{
			i:            4000,
			numbers:      "4000.",
			lettersLower: "ewv.",
			lettersUpper: "EWV.",
			romanLower:   "4000.",
			romanUpper:   "4000.",
		},
	}

	for _, tt := range tests {
		t.Run(strconv.Itoa(tt.i), func(t *testing.T) {
			t.Run("Numbers", func(t *testing.T) {
				l := Numbers{i: tt.i - 1}
				got := l.Next()
				if got != tt.numbers {
					t.Errorf("Unexpected result: %s", got)
				}
			})
			t.Run("LettersLower", func(t *testing.T) {
				l := (&LettersLower{}).New().(*letters) //nolint:errcheck // okay in test
				l.i = tt.i - 1
				got := l.Next()
				if got != tt.lettersLower {
					t.Errorf("Unexpected result: %s", got)
				}
			})
			t.Run("LettersUpper", func(t *testing.T) {
				l := (&LettersUpper{}).New().(*letters) //nolint:errcheck // okay in test
				l.i = tt.i - 1
				got := l.Next()
				if got != tt.lettersUpper {
					t.Errorf("Unexpected result: %s", got)
				}
			})
			t.Run("RomanLower", func(t *testing.T) {
				l := (&RomanLower{}).New().(*roman) //nolint:errcheck // okay in test
				l.i = tt.i - 1
				got := l.Next()
				if got != tt.romanLower {
					t.Errorf("Unexpected result: %s", got)
				}
			})
			t.Run("RomanUpper", func(t *testing.T) {
				l := (&RomanUpper{}).New().(*roman) //nolint:errcheck // okay in test
				l.i = tt.i - 1
				got := l.Next()
				if got != tt.romanUpper {
					t.Errorf("Unexpected result: %s", got)
				}
			})
		})
	}
}
