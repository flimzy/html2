package html2

import (
	"bytes"
	"errors"
	"fmt"
	"io"

	"golang.org/x/net/html"
)

type err int

var _ error = err(0)

func (e err) Error() string {
	switch e {
	case ErrWalkStop:
		return "walking stopped"
	case ErrWalkSkipChildren:
		return "chilren skipped"
	}
	panic(fmt.Sprintf("unrecognized error value: %v", int(e)))
}

const (
	// ErrWalkStop indicates no more walking needed.
	ErrWalkStop err = iota + 1
	// ErrWalkSkipChildren indicates that the children of this node should not
	// be walked.
	ErrWalkSkipChildren
)

// Renderer is the interface for a generic HTML renderer.
type Renderer interface {
	// Render should render n by writing to w. It is not responsible for walking
	// child nodes, which means that it is perfectly valid for this function to
	// do nothing for certain node types.
	//
	// Render is called twice for each node, first with entering true, before
	// processing any children, then a second time with entering false, after
	// processing all children.
	//
	// As special cases, if ErrWalkStop or ErrWalkSkipChildren values are
	// returned,then walking is stopped (without error), or children of the
	// current node are skipped, respectively.
	//
	// Returning WalkSkipChildren when entering=false will cause a panic.
	Render(w Writer, n *html.Node, entering bool) error
}

// RenderFunc allows handling a simple function as a Renderer.
type RenderFunc func(Writer, *html.Node, bool) error

// Render satisfies the Renderer interface.
func (rf RenderFunc) Render(w Writer, n *html.Node, entering bool) error {
	return rf(w, n, entering)
}

// Render renders walks n, passing each node to r for rendering.
func Render(w io.Writer, n *html.Node, r Renderer) error {
	wr := &writer{w: w}
	err := walk(n, func(n *html.Node, entering bool) error {
		return r.Render(wr, n, entering)
	})
	if !errors.Is(err, ErrWalkStop) {
		return err
	}
	return nil
}

// RenderToString works like Render, but returns a string.
func RenderToString(n *html.Node, r Renderer) (string, error) {
	buf := &bytes.Buffer{}
	err := Render(buf, n, r)
	return buf.String(), err
}

// Writer wraps a standard io.Writer, to add a few extensions useful within
// a Renderer.
type Writer interface {
	io.Writer
	io.ByteWriter
	io.StringWriter
	LastByte() (byte, bool)
}

type writer struct {
	w    io.Writer
	last *byte
}

var _ Writer = &writer{}

func (w *writer) setLast(c byte) {
	w.last = &c
}

func (w *writer) Write(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}
	w.setLast(p[len(p)-1])
	return w.w.Write(p)
}

func (w *writer) WriteByte(c byte) error {
	w.setLast(c)
	_, err := w.Write([]byte{c})
	return err
}

func (w *writer) WriteString(s string) (int, error) {
	return w.Write([]byte(s))
}

func (w *writer) LastByte() (byte, bool) {
	if w.last == nil {
		return 0, false
	}
	return *w.last, true
}

func walk(n *html.Node, r func(*html.Node, bool) error) error {
	if n == nil {
		return nil
	}
	skipChildren := false
	// First the self, entering=true
	if err := r(n, true); err != nil {
		if !errors.Is(err, ErrWalkSkipChildren) {
			return err
		}
		skipChildren = true
	}
	// Then any children
	if !skipChildren {
		if err := walk(n.FirstChild, r); err != nil {
			return err
		}
	}
	// Then call for self again, entering=false
	if err := r(n, false); err != nil {
		if errors.Is(err, ErrWalkSkipChildren) {
			panic("WalkSkipChildren returned after children processed")
		}
		return err
	}
	// Then any remaining siblings
	return walk(n.NextSibling, r)
}
